<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
  <body>
    <?php
    $_VARS = array_merge($_GET, $_POST);
    $scriptName = sprintf("StoreComment.php%s", isset($_VARS['reqId']) ? "?reqId=" . $_VARS['reqId'] : "");
    include("PHPprinter.php");
    $startTime = getMicroTime();

    $nickname = isset($_VARS['nickname']) ? $_VARS['nickname'] : null;
    $password = isset($_VARS['password']) ? $_VARS['password'] : null;
    $storyId = isset($_VARS['storyId']) ? $_VARS['storyId'] : null;
    $parent = isset($_VARS['parent']) ? $_VARS['parent'] : null;
    $subject = isset($_VARS['subject']) ? substr($_VARS['subject'], 0, 100) : null;
    $body = isset($_VARS['body']) ? $_VARS['body'] : null;
    $comment_table = isset($_VARS['comment_table']) ? $_VARS['comment_table'] : null;

    if ($storyId == null)
    {
       printError($scriptName, $startTime, "StoreComment", "You must provide a story identifier!<br>");
       exit();
    }
    if ($parent == null)
    {
       printError($scriptName, $startTime, "StoreComment", "You must provide a follow up identifier!<br>");
       exit();
    }
    if ($subject == null)
    {
       printError($scriptName, $startTime, "StoreComment", "You must provide a comment subject!<br>");
       exit();
    }
    if ($body == null)
    {
       printError($scriptName, $startTime, "StoreComment", "<h3>You must provide a comment body!<br></h3>");
       exit();
    }
    if ($comment_table == null)
    {
       printError($scriptName, $startTime, "Viewing comment", "You must provide a comment table!<br>");
       exit();
    }

    getDatabaseLink($link);

    printHTMLheader("RUBBoS: Comment submission result");

    print("<center><h2>Comment submission result:</h2></center><p>\n");

    // Authenticate the user
    $userId = authenticate($nickname, $password, $link);
    if ($userId == 0)
      print("Comment posted by the 'Anonymous Coward'<br>\n");
    else
      print("Comment posted by user #$userId<br>\n");

    // Add comment to database
    $now = date("Y:m:d H:i:s");
    $query = "INSERT INTO $comment_table VALUES (NULL, $userId, $storyId, $parent, 0, 0, '$now', \"$subject\", \"$body\")";
    $result = mysql_query($query, $link) or die("ERROR: Failed to insert new comment in database: " . $query);
    $query = "UPDATE $comment_table SET childs=childs+1 WHERE id=$parent";
    $result = mysql_query($query, $link) or die("ERROR: Failed to update parent childs in database: " . $query);

    print("Your comment has been successfully stored in the $table database table<br>\n");

    mysql_close($link);

    printHTMLfooter($scriptName, $startTime);
    ?>
  </body>
</html>
