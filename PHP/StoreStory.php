<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
  <body>
    <?php
    $_VARS = array_merge($_GET, $_POST);
    $scriptName = sprintf("SubmitStory.php%s", isset($_VARS['reqId']) ? "?reqId=" . $_VARS['reqId'] : "");
    include("PHPprinter.php");
    $startTime = getMicroTime();

    $nickname = isset($_VARS['nickname']) ? $_VARS['nickname'] : null;
    $password = isset($_VARS['password']) ? $_VARS['password'] : null;
    $title = isset($_VARS['title']) ? substr($_VARS['password'], 0, 100) : null;
    $body = isset($_VARS['body']) ? $_VARS['body'] : null;
    $category = isset($_VARS['category']) ? $_VARS['category'] : null;

    if ($title == null)
    {
       printError($scriptName, $startTime, "SubmitStory", "You must provide a story title!<br>");
       exit();
    }
    if ($body == null)
    {
       printError($scriptName, $startTime, "SubmitStory", "<h3>You must provide a story body!<br></h3>");
       exit();
    }
    if ($category == null)
    {
       printError($scriptName, $startTime, "SubmitStory", "<h3>You must provide a category !<br></h3>");
       exit();
    }

    getDatabaseLink($link);

    printHTMLheader("RUBBoS: Story submission result");

    print("<center><h2>Story submission result:</h2></center><p>\n");

    // Authenticate the user
    $userId = 0;
    $access = 0;
    if (($nickname != null) && ($password != null))
    {
      $result = mysql_query("SELECT id,access FROM users WHERE nickname=\"$nickname\" AND password=\"$password\"", $link) or die("ERROR: Authentication query failed");
      if (mysql_num_rows($result) != 0)
      {
        $row = mysql_fetch_array($result);
        $userId = $row["id"];
        $access = $row["access"];
      }
      mysql_free_result($result);
    }

    $table = "submissions";
    if ($userId == 0)
      print("Story stored by the 'Anonymous Coward'<br>\n");
    else
    {
      if ($access == 0)
        print("Story submitted by regular user #$userId<br>\n");
      else
      {
        print("Story posted by author #$userId<br>\n");
        $table = "stories";
      }
    }

    // Add story to database
    $now = date("Y:m:d H:i:s");
    $query = "INSERT INTO $table VALUES (NULL, \"$title\", \"$body\", '$now', $userId, $category)";
    $result = mysql_query($query, $link) or die("ERROR: Failed to insert new story in database: " + $query);

    print("Your story has been successfully stored in the $table database table<br>\n");

    mysql_close($link);

    printHTMLfooter($scriptName, $startTime);
    ?>
  </body>
</html>
