<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
  <body>
    <?php
    $_VARS = array_merge($_GET, $_POST);
    $scriptName = sprintf("RegisterUser.php%s", isset($_VARS['reqId']) ? "?reqId=" . $_VARS['reqId'] : "");
    include("PHPprinter.php");
    $startTime = getMicroTime();

    $firstname = isset($_VARS['firstname']) ? $_VARS['firstname'] : null;
    $lastname = isset($_VARS['lastname']) ? $_VARS['lastname'] : null;
    $nickname = isset($_VARS['nickname']) ? $_VARS['nickname'] : null;
    $email = isset($_VARS['email']) ? $_VARS['email'] : null;
    $password = isset($_VARS['password']) ? $_VARS['password'] : null;

    if ($firstname == null)
    {
       printError($scriptName, $startTime, "Register user", "You must provide a first name!<br>");
       exit();
    }
    if ($lastname == null)
    {
       printError($scriptName, $startTime, "Register user", "You must provide a last name!<br>");
       exit();
    }
    if ($nickname == null)
    {
       printError($scriptName, $startTime, "Register user", "You must provide a nick name!<br>");
       exit();
    }
    if ($email == null)
    {
       printError($scriptName, $startTime, "Register user", "You must provide an email address!<br>");
       exit();
    }
    if ($password == null)
    {
       printError($scriptName, $startTime, "Register user", "You must provide a password!<br>");
       exit();
    }

    getDatabaseLink($link);

    // Check if the nick name already exists
    $nicknameResult = mysql_query("SELECT * FROM users WHERE nickname=\"$nickname\"", $link) or die("ERROR: Nickname query failed");
    if (mysql_num_rows($nicknameResult) > 0)
    {
      printError($scriptName, $startTime, "Register user", "The nickname you have choosen is already taken by someone else. Please choose a new nickname.<br>\n");
      mysql_free_result($nicknameResult);
      exit();
    }
    mysql_free_result($nicknameResult);

    // Add user to database
    $now = date("Y:m:d H:i:s");
    $result = mysql_query("INSERT INTO users VALUES (NULL, \"$firstname\", \"$lastname\", \"$nickname\", \"$password\", \"$email\", 0, 0, '$now')", $link) or die("ERROR: Failed to insert new user in database.");

    $result = mysql_query("SELECT * FROM users WHERE nickname=\"$nickname\"", $link) or die("ERROR: Query user failed");
    $row = mysql_fetch_array($result);

    printHTMLheader("RUBBoS: Welcome to $nickname");
    print("<h2>Your registration has been processed successfully</h2><br>\n");
    print("<h3>Welcome $nickname</h3>\n");
    print("RUBBoS has stored the following information about you:<br>\n");
    print("First Name : ".$row["firstname"]."<br>\n");
    print("Last Name  : ".$row["lastname"]."<br>\n");
    print("Nick Name  : ".$row["nickname"]."<br>\n");
    print("Email      : ".$row["email"]."<br>\n");
    print("Password   : ".$row["password"]."<br>\n");
    print("<br>The following information has been automatically generated by RUBBoS:<br>\n");
    print("User id       :".$row["id"]."<br>\n");
    print("Creation date :".$row["creation_date"]."<br>\n");
    print("Rating        :".$row["rating"]."<br>\n");
    print("Access        :".$row["access"]."<br>\n");

    mysql_free_result($result);
    mysql_close($link);

    printHTMLfooter($scriptName, $startTime);
    ?>
  </body>
</html>
