from dateutil import parser

i = 0
new_value = "1999-03-23"
fw = open('data/old_comments_fixed.data', 'w')

with open('data/old_comments.data', 'r') as f:
    for line in f:
        i += 1
        fields = line.split('\t')
        try:
            parser.parse(fields[6])
        except:
            print("Error @ line %d: parsing '%s' -> new value: '%s'" % (i, fields[6], new_value))
            fields[6] = new_value
        fw.write("\t".join(fields))
        # if i > 20:
        #     break

        if i % 100000 == 0:
            print("%d lines processed" % i)

fw.close()