package edu.rice.rubbos.client;

import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.ScheduledFuture;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;
// import redis.clients.jedis.Jedis;
import org.json.JSONObject;
import java.net.HttpURLConnection;
import java.net.URL;
import java.io.OutputStreamWriter;
import java.net.MalformedURLException;
import java.net.ProtocolException;
import java.io.IOException;
import java.net.InetAddress;
import java.net.NetworkInterface;
import java.net.UnknownHostException;
import java.util.Enumeration;
import java.net.SocketException;
import java.net.Inet4Address;
import org.apache.commons.math3.stat.descriptive.DescriptiveStatistics;
import org.apache.commons.math3.stat.StatUtils;

/**
 * This class provides thread-safe statistics. Each statistic entry is composed as follow:
 * <pre>
 * count     : statistic counter
 * error     : statistic error counter
 * minTime   : minimum time for this entry (automatically computed)
 * maxTime   : maximum time for this entry (automatically computed)
 * totalTime : total time for this entry
 * </pre>
 *
 * @author <a href="mailto:cecchet@rice.edu">Emmanuel Cecchet</a> and <a href="mailto:julie.marguerite@inrialpes.fr">Julie Marguerite</a>
 * @version 1.0
 */

public class Stats
{
  private int nbOfStats;
  // private int count[];
  private int error[];
  // private long minTime[];
  // private long maxTime[];
  // private long totalTime[];
  private DescriptiveStatistics pageStats[];
  private int  nbSessions;   // Number of sessions succesfully ended
  private long sessionsTime; // Sessions total duration
  private String ip;
  private String hostname;

  // Publish Stats to REDIS
  private int publishStatsPeriod;              // publish interval
  private final ScheduledExecutorService scheduler; // scheduler for periodic publishing
  // private Jedis jedis;                              // Java REDIS client

  private DescriptiveStatistics rtStats;
  private int percentile;

  private final class PublishStatsTask implements Runnable {
    @Override public void run() {
      Stats.this.publishStats();
      Stats.this.reset();
    }
  }

  /**
   * Creates a new <code>Stats</code> instance.
   * The entries are reset to 0.
   *
   * @param NbOfStats number of entries to create
   */
  public Stats(int NbOfStats)
  {
    this(NbOfStats, false);
  }

  public Stats(int NbOfStats, boolean publish)
  {
    // set Stats parameters
    publishStatsPeriod = 10;
    percentile = 95;

    rtStats = new DescriptiveStatistics();

    nbOfStats = NbOfStats;
    // count = new int[nbOfStats];
    error = new int[nbOfStats];
    pageStats = new DescriptiveStatistics[nbOfStats];
    // minTime = new long[nbOfStats];
    // maxTime = new long[nbOfStats];
    // totalTime = new long[nbOfStats];
    reset();

    scheduler = Executors.newScheduledThreadPool(1);
    if (publish) {
      // get ip
      this.setNetworkInfo();
      System.out.println("Setting up schedule");
      Runnable publishStatsTask = new PublishStatsTask();
      ScheduledFuture<?> publishStats = scheduler.scheduleWithFixedDelay(
        publishStatsTask, publishStatsPeriod, publishStatsPeriod, TimeUnit.SECONDS
      );
    }
  }


  /**
   * Gather network information to publish results (assumes single IPv4 address)
   **/
  public synchronized void setNetworkInfo() {
    try {
        Enumeration<NetworkInterface> interfaces = NetworkInterface.getNetworkInterfaces();
        while (interfaces.hasMoreElements()) {
            NetworkInterface iface = interfaces.nextElement();
            // filters out 127.0.0.1 and inactive interfaces
            if (iface.isLoopback() || !iface.isUp())
                continue;

            Enumeration<InetAddress> addresses = iface.getInetAddresses();
            while(addresses.hasMoreElements()) {
                InetAddress addr = addresses.nextElement();
                if (addr instanceof Inet4Address) {
                  this.ip = addr.getHostAddress();
                  this.hostname = addr.getHostName();
                  System.out.println("Address: " + ip + " / " + hostname);
                }
            }
        }
    } catch (SocketException e) {
        throw new RuntimeException(e);
    }
  }


  /**
   * Resets all entries to 0
   */
  public synchronized void reset()
  {
    int i;

    for (i = 0 ; i < nbOfStats ; i++)
    {
      // count[i] = 0;
      error[i] = 0;
      // minTime[i] = Long.MAX_VALUE;
      // maxTime[i] = 0;
      // totalTime[i] = 0;
      pageStats[i] = new DescriptiveStatistics();
    }
    nbSessions = 0;
    sessionsTime = 0;
    rtStats.clear();
  }

  public synchronized void publishStats() {
    try {
      // 1) which response time should we report? All?
      //  * Lets do an average of all pages? The most frequent page? Longest page? Reference page?
      // 2) UDP Request? JSON?
      //  * Option 1 - Send to REDIS: redis is actually running on the physical machine. It's address is available, but not known
      //  * Option 2 - Send to the agent: accessible, but accepts only LAMA events
      //  * Option 3 - Send to the monitor (should be part of the agent): currently not accessible.
      //  * Decision: let us create a small listener that receives a UDP JSON and dumps it to redis...

      // // Connect to REDIS @ 172.16.0.2
      // String channel = "VMTest";
      // String values = 3;
      // jedis.publish(channel, values);

      // System.out.println("Number of stats: " + nbOfStats);
      if (nbOfStats > 0) {
        // find the maximum entry
        long maxCount = pageStats[0].getN();
        int maxIndex = 0;
        // long allTime = 0;
        // long allCount = 0;
        // long allMax = maxTime[0];
        // long allMin = minTime[0];
        long allErrors = 0;
        // System.out.println("Computing aggregated stats");
        for (int i = 0; i < nbOfStats; i++) {
        //     if (count[i] > maxVal) {
        //       maxVal = count[i];
        //       maxIndex = i;
        //     }
          if (pageStats[i].getN() > maxCount) {
            maxCount = pageStats[i].getN();
            maxIndex = i;
          }
        //
        //     if (maxTime[i] > allMax)
        //       allMax = maxTime[i];
        //
        //     if (minTime[i] < allMin)
        //       allMin = minTime[i];
        //
        //     allTime += totalTime[i];
        //     allCount += count[i];
          allErrors += error[i];
        }

        // prepare json
        JSONObject data = null;
        // System.out.println("New JSON Object");
        try {
          data = new JSONObject();
        } catch (Exception e) {
          e.printStackTrace();
          return;
        }
        data.put("name", this.hostname);
        data.put("timestamp", System.currentTimeMillis()/1000);
        JSONObject metrics = new JSONObject();
        JSONObject responseTime = new JSONObject();

        int[] pagesToPublish = new int[]{0, 2, 7, 9, 10, 12};
        // System.out.println("Prepare JSON - pages: " + pagesToPublish.length);
        for (int j = 0; j < pagesToPublish.length; j++) {
          int page = pagesToPublish[j];
          JSONObject values = new JSONObject();
          long n = pageStats[page].getN();
          values.put("min", n > 0 ? pageStats[page].getMin() : 0);
          // values.put("max", n > 0 ? pageStats[page].getMax() : 0);
          values.put("avg", n > 0 ? pageStats[page].getMean() : 0);
          values.put(percentile + "perc", n > 0 ? pageStats[page].getPercentile(percentile) : 0);
          values.put("requests", ((double)pageStats[page].getN() + error[page])/ publishStatsPeriod);
          values.put("errors", ((double)error[page])/ publishStatsPeriod);
          values.put(percentile + "avg", n > 1 ? StatUtils.mean(pageStats[page].getSortedValues(), 0, (int) n * percentile / 100) : values.getInt("avg"));
          responseTime.put("ref_" + page, values);

        }

        JSONObject values = new JSONObject();
        long n = rtStats.getN();
        values.put("min", n > 0 ? rtStats.getMin() : 0);
        // values.put("max", n > 0 ? rtStats.getMax() : 0);
        values.put("avg", n > 0 ? rtStats.getMean() : 0);
        values.put(percentile + "perc", n > 0 ? rtStats.getPercentile(percentile) : 0);
        responseTime.put("total", values);

        metrics.put("response_time", responseTime);

        JSONObject requests = new JSONObject();
        requests.put("most_requested", maxIndex);
        requests.put("num_requests", ((double)rtStats.getN() + allErrors)/publishStatsPeriod);
        requests.put("errors", ((double)allErrors)/publishStatsPeriod);
        metrics.put("requests", requests);

        data.put("metrics", metrics);

        System.out.println("Posting data: " + data.toString());

        URL url = null;
        try {
          url = new URL("http://192.168.0.2:8400/");
        } catch (MalformedURLException e) {
          System.err.println("ERROR/Bad URL: " + e.getMessage());
          return;
        }

        OutputStreamWriter osw = null;
        try {
          HttpURLConnection connection = (HttpURLConnection) url.openConnection();
          connection.setRequestMethod("POST");
          connection.setDoOutput(true);
          // connection.setRequestProperty("Content-Type", "application/json");
          connection.setRequestProperty("Content-Type", "application/json; charset=UTF-8");
          connection.setRequestProperty("Accept", "application/json");

          osw = new OutputStreamWriter(connection.getOutputStream());
          // metric: app_response_time
          // submetrics: ref, most_used, total
          // values: cnt, min, avg, max
          // osw.write(String.format("{\"pos\":{\"left\":%1$d,\"top\":%2$d}}", random.nextInt(30), random.nextInt(20)));
          // { "timestamp": <timestamp>, metrics: {"response_time": {"ref": [<vals>]}
          // }
          osw.write(data.toString());
          osw.flush();

          System.out.println("Sent message: " + connection.getResponseCode());
        } catch (ProtocolException e) {
          System.out.println("ERROR/Protocol exception: " + e.getMessage());
          return;
        } catch (IOException e) {
          System.out.println("ERROR/IO exception: " + e.getMessage());
          return;
        } finally {
          if (osw != null) {
            try {
              osw.close();
            } catch (IOException e) {
              System.err.println("ERROR/IO exception when closing: " + e.getMessage());
            }
          }
        }
      }
    } catch (Exception e) {
      System.out.println("ERROR/Exception: " + e.getMessage());
      e.printStackTrace();
    }

  }

  /**
   * Add a session duration to the total sessions duration and
   * increase the number of succesfully ended sessions.
   *
   * @param time duration of the session
   */
  public synchronized void addSessionTime(long time)
  {
    nbSessions++;
    if (time < 0)
    {
      System.err.println("Negative time received in Stats.addSessionTime("+time+")<br>\n");
      return;
    }
    sessionsTime = sessionsTime + time;
  }

 /**
   * Increment the number of succesfully ended sessions.
   */
  public synchronized void addSession()
  {
    nbSessions++;
  }


  // /**
  //  * Increment an entry count by one.
  //  *
  //  * @param index index of the entry
  //  */
  // public synchronized void incrementCount(int index)
  // {
  //   count[index]++;
  // }


  /**
   * Increment an entry error by one.
   *
   * @param index index of the entry
   */
  public synchronized void incrementError(int index)
  {
    error[index]++;
  }


  /**
   * Add a new time sample for this entry. <code>time</code> is added to total time
   * and both minTime and maxTime are updated if needed.
   *
   * @param index index of the entry
   * @param time time to add to this entry
   */
  public synchronized void updateTime(int index, long time)
  {
    if (time < 0)
    {
      System.err.println("Negative time received in Stats.updateTime("+time+")<br>\n");
      return ;
    }
    pageStats[index].addValue(time);
    // totalTime[index] += time;
    // if (time > maxTime[index])
    //   maxTime[index] = time;
    // if (time < minTime[index])
    //   minTime[index] = time;

    rtStats.addValue(time);
  }


  /**
   * Get current count of an entry
   *
   * @param index index of the entry
   *
   * @return entry count value
   */
  public synchronized long getCount(int index)
  {
    // return count[index];
    return pageStats[index].getN();
  }


  /**
   * Get current error count of an entry
   *
   * @param index index of the entry
   *
   * @return entry error value
   */
  public synchronized int getError(int index)
  {
    return error[index];
  }


  /**
   * Get the minimum time of an entry
   *
   * @param index index of the entry
   *
   * @return entry minimum time
   */
  public synchronized long getMinTime(int index)
  {
    return (long)pageStats[index].getMin();
    // return minTime[index];
  }


  /**
   * Get the maximum time of an entry
   *
   * @param index index of the entry
   *
   * @return entry maximum time
   */
  public synchronized long getMaxTime(int index)
  {
    return (long)pageStats[index].getMax();
    // return maxTime[index];
  }


  /**
   * Get the total time of an entry
   *
   * @param index index of the entry
   *
   * @return entry total time
   */
  public synchronized long getTotalTime(int index)
  {
    return (long)pageStats[index].getSum();
    // return totalTime[index];
  }


  /**
   * Get the total number of entries that are collected
   *
   * @return total number of entries
   */
  public int getNbOfStats()
  {
    return nbOfStats;
  }


  /**
   * Adds the entries of another Stats object to this one.
   *
   * @param anotherStat stat to merge with current stat
   */
  public synchronized void merge(Stats anotherStat)
  {
    if (this == anotherStat)
    {
      System.out.println("You cannot merge a stats with itself");
      return;
    }
    if (nbOfStats != anotherStat.getNbOfStats())
    {
      System.out.println("Cannot merge stats of differents sizes.");
      return;
    }
    for (int i = 0 ; i < nbOfStats ; i++)
    {
      // count[i] += anotherStat.getCount(i);
      error[i] += anotherStat.getError(i);
      // if (minTime[i] > anotherStat.getMinTime(i))
      //   minTime[i] = anotherStat.getMinTime(i);
      // if (maxTime[i] < anotherStat.getMaxTime(i))
      //   maxTime[i] = anotherStat.getMaxTime(i);
      // totalTime[i] += anotherStat.getTotalTime(i);
    }
    nbSessions   += anotherStat.nbSessions;
    sessionsTime += anotherStat.sessionsTime;
  }


  /**
   * Display an HTML table containing the stats for each state.
   * Also compute the totals and average throughput
   *
   * @param title table title
   * @param sessionTime total time for this session
   * @param exclude0Stat true if you want to exclude the stat with a 0 value from the output
   */
  public void displayStats(String title, long sessionTime, boolean exclude0Stat)
  {
    long counts = 0;
    int errors = 0;
    long time = 0;

    System.out.println("<br><h3>"+title+" statistics</h3><p>");
    System.out.println("<TABLE BORDER=1>");
    System.out.println("<THEAD><TR><TH>State name<TH>% of total<TH>Count<TH>Errors<TH>Minimum Time<TH>Maximum Time<TH>Average Time<TBODY>");
    // Display stat for each state
    for (int i = 0 ; i < getNbOfStats() ; i++)
    {
      counts += pageStats[i].getN();
      errors += error[i];
      time += pageStats[i].getSum();
    }

    for (int i = 0 ; i < getNbOfStats() ; i++)
    {
      long count = pageStats[i].getN();

      if ((exclude0Stat && count != 0) || (!exclude0Stat))
      {
        System.out.print("<TR><TD><div align=left>"+TransitionTable.getStateName(i)+"</div><TD><div align=right>");
        if ((counts > 0) && (count > 0))
          System.out.print(100*count/counts+" %");
        else
          System.out.print("0 %");
        System.out.print("</div><TD><div align=right>"+(count + error[i])+"</div><TD><div align=right>");
        if (error[i] > 0)
          System.out.print("<B>"+error[i]+"</B>");
        else
          System.out.print(error[i]);
        System.out.print("</div><TD><div align=right>");
        System.out.print(pageStats[i].getMin());
        System.out.print(" ms</div><TD><div align=right>"+pageStats[i].getMax()+" ms</div><TD><div align=right>");
        if (count > 0)
          System.out.println(pageStats[i].getMean() + " ms</div>");
        else
           System.out.println("0 ms</div>");
      }
    }

    // Display total
    if (counts > 0)
    {
      System.out.print("<TR><TD><div align=left><B>Total</B></div><TD><div align=right><B>100 %</B></div><TD><div align=right><B>"+counts+
                       "</B></div><TD><div align=right><B>"+errors+ "</B></div><TD><div align=center>-</div><TD><div align=center>-</div><TD><div align=right><B>");
      counts += errors;
      System.out.println(time/counts+" ms</B></div>");
      // Display stats about sessions
      System.out.println("<TR><TD><div align=left><B>Average throughput</div></B><TD colspan=6><div align=center><B>"+1000*counts/sessionTime+" req/s</B></div>");
      System.out.println("<TR><TD><div align=left>Completed sessions</div><TD colspan=6><div align=left>"+nbSessions+"</div>");
      System.out.println("<TR><TD><div align=left>Total time</div><TD colspan=6><div align=left>"+sessionsTime/1000L+" seconds</div>");
      System.out.print("<TR><TD><div align=left><B>Average session time</div></B><TD colspan=6><div align=left><B>");
      if (nbSessions > 0)
        System.out.print(sessionsTime/(long)nbSessions/1000L+" seconds");
      else
        System.out.print("0 second");
      System.out.println("</B></div>");
    }
    System.out.println("</TABLE><p>");
  }


}
