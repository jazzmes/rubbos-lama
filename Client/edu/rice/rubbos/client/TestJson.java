import org.json.JSONObject;

class TestJson {
  public static void main(String[] args) {
    System.out.println("Hello JSON!"); // Display the string.
    JSONObject json = new JSONObject();
    System.out.println("Goodbye JSON!"); // Display the string.
  }
}
